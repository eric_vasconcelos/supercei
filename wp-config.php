<?php
/** 
 * As configurações básicas do WordPress.
 *
 * Esse arquivo contém as seguintes configurações: configurações de MySQL, Prefixo de Tabelas,
 * Chaves secretas, Idioma do WordPress, e ABSPATH. Você pode encontrar mais informações
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. Você pode obter as configurações de MySQL de seu servidor de hospedagem.
 *
 * Esse arquivo é usado pelo script ed criação wp-config.php durante a
 * instalação. Você não precisa usar o site, você pode apenas salvar esse arquivo
 * como "wp-config.php" e preencher os valores.
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar essas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'supercei');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Conjunto de caracteres do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8');

/** O tipo de collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * Você pode alterá-las a qualquer momento para desvalidar quaisquer cookies existentes. Isto irá forçar todos os usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E&jwGuE+`y<PebIY53!%{2Pu8dfklL]5tR$MJW@VrEb}6kye0WZOjH(F3m&Tj+[m');
define('SECURE_AUTH_KEY',  'zBk?;Ff.|fG.I}IaE0}0-xI8jH2L~I)bjNkI#4#CeY1i&:wKDsn4L~ax<7&gO:@h');
define('LOGGED_IN_KEY',    'wLN{7e;!:!Kdi&f#^p9Gxmvu@X +tHCbkcQ@o$]?EGZF`p~[DE}UwQopv%h;$6ci');
define('NONCE_KEY',        'zYQKX]oU+3-S>`YqH|YVr1_0ewZZu*uyW_K+V(9&CVXFR`PL OL21Tm5(0`z)|{&');
define('AUTH_SALT',        'f y!%}EqW_E!:y|Zn|s;n`bmT{XQAx/m[HeQbCA@(g!jn<Pi2ClE[+F{-`@Vr;wz');
define('SECURE_AUTH_SALT', 'Z,gpj]apnb?vNy-+QTe*,;8hx^xe:gtNd1{/.;Ll*Of^[%u|$+wG@|^L><3ltc!S');
define('LOGGED_IN_SALT',   'ft;do+9mI-)D|0}U@x,Yn?3Pab=FC$A:XW;J$vdrr*$U73OKr|+.l.f,V|ly9UC@');
define('NONCE_SALT',       'h+mtbj$q5 XoPuJFYIu8b6D*&*dDL-GN#Y(=8~ZwBH7fu:y=Se+$B3TKQa-IH#K<');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der para cada um um único
 * prefixo. Somente números, letras e sublinhados!
 */
$table_prefix  = 'sc_';


/**
 * Para desenvolvedores: Modo debugging WordPress.
 *
 * altere isto para true para ativar a exibição de avisos durante o desenvolvimento.
 * é altamente recomendável que os desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
	
/** Configura as variáveis do WordPress e arquivos inclusos. */
require_once(ABSPATH . 'wp-settings.php');
