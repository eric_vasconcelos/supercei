<!DOCTYPE html>
<!--[if IE 7]>
<html class="no-js ie ie7 lt-ie9 lt-ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="no-js ie ie8 lt-ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" rel="shortcut icon" />
	<link href='http://fonts.googleapis.com/css?family=Dosis:400,700,800' rel='stylesheet' type='text/css'>
	
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<header id="header" role="navigation">
		<div class="container">
			<div class="row">
				<div class="col-4">
					<div id="logo">
						<a href="<?php echo get_site_url(); ?>/">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-supermercados-supercei.png" width="283" height="73" alt="Logo Supercei">
						</a>
					</div><!-- #logo -->
				</div><!-- .col-4 -->
			</div><!-- .row -->

			<div class="row">
				<div class="col-12">
					<nav id="menu-navigation" class="cf" role="navigation">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'main-menu',
									'depth'          => 1,
									'container'      => false,
									'menu_class'     => 'nav'
								)
							);
						?>

						<div class="location">
							<p>você está em </p>
							<div class="wrap-select">
								<?php 
									$args = array(
										'show_option_all'    => '',
										'show_option_none'   => '',
										'orderby'            => 'NAME', 
										'order'              => 'ASC',
										'show_count'         => 0,
										'hide_empty'         => 1, 
										'child_of'           => 0,
										'exclude'            => '',
										'echo'               => 1,
										'selected'           => 0,
										'hierarchical'       => 0, 
										'name'               => 'cidades',
										'id'                 => 'select-cidades-menu',
										'class'              => 'cities',
										'depth'              => 0,
										'tab_index'          => 0,
										'taxonomy'           => 'cidade',
										'hide_if_empty'      => true,
									); 

									wp_dropdown_categories( $args );
								?>
							</div>
						</div>
					</nav>
				</div>
			</div>
			
		</div>
	</header><!-- #header -->

