/* jshint node:true */
module.exports = function( grunt ) {
	'use strict';
	require( 'load-grunt-tasks' )( grunt );
	var odinConfig = {
		// gets the package vars
		pkg: grunt.file.readJSON( 'package.json' ),

		// templates
		dirs: {
			css:    '../assets/css',
			js: 	'../assets/js',
			images: '../assets/images',
			fonts: 	'../assets/fonts',
			core: 	'../core',
			tmp: 	'tmp'
		},

		// javascript jshint
		jshint: {
			options: {
				jshintrc: '../.jshintrc'
			},
			all: [
				'js/main.js'
			]
		},

		// uglify concatenar e minificar
		uglify: {
			dist: {
				files: {
					'<%= dirs.js %>/main.min.js': ['js/libs/*.js', 'js/main.js']
				}
			}
		},

		// Less
		less: {
		  	development: {
			    options: {
			    	compress: true,
		          	yuicompress: true,
		          	optimization: 2
		    	},
			    files: {
			      	"<%= dirs.css %>/style.css": "less/style.less"
			    }
		  	}
		},

		// watch for changes and trigger compass, jshint and uglify
		watch: {
			styles: {
		        files: ['less/*.less'], // which files to watch
		        tasks: ['less'],
		        options: {
		          	nospawn: true,
		        }
	      	},

			js: {
				files: ['js/*.js', 'Gruntfile.js'],
				tasks: ['uglify'] //'jshint', 
			}
		},

		// image optimization
		imagemin: {
			dist: {
				options: {
					optimizationLevel: 7,
					progressive: true
				},
				files: [{
					expand: true,
					cwd: '<%= dirs.images %>/',
					src: '**/*.{png,jpg,gif}',
					dest: '<%= dirs.images %>/'
				}]
			}
		},
		// deploy via rsync
		rsync: {
			options: {
				args: ['--verbose'],
				exclude: [
					'**.DS_Store',
					'**Thumbs.db',
					'.editorconfig',
					'.git/',
					'.gitignore',
					'.jshintrc',
					'src/',
					'README.md'
				],
				recursive: true,
				syncDest: true
			},
			staging: {
				options: {
					src: '../',
					dest: '~/PATH/wp-content/themes/odin',
					host: 'user@host.com'
				}
			},
			production: {
				options: {
					src: '../',
					dest: '~/PATH/wp-content/themes/odin',
					host: 'user@host.com'
				}
			}
		},
		// ftp deploy
		'ftp-deploy': {
			build: {
				auth: {
					host: 'ftp.SEU-SITE.com',
					port: 21,
					authKey: 'key_for_deploy'
				},
				src: '../',
				dest: '/PATH/wp-content/themes/odin',
				exclusions: [
					'../**.DS_Store',
					'../**Thumbs.db',
					'../.git/*',
					'../.gitignore',
					'../src/*',
					'../src/node_modules/*',
					'../src/.ftppass',
					'../src/Gruntfile.js',
					'../src/config.rb',
					'../src/package.json',
					'../README.md',
					'../**/README.md'
				]
			}
		}
	};
	// Initialize Grunt Config
	// --------------------------
	grunt.initConfig( odinConfig );
	// Register Tasks
	// --------------------------
	// Default Task
	grunt.registerTask( 'default', [
		'jshint',
		'uglify'
	] );
	// Optimize Images Task
	grunt.registerTask( 'optimize', ['imagemin'] );
	// Deploy Tasks
	grunt.registerTask( 'ftp', ['ftp-deploy'] );
	// Short aliases
	grunt.registerTask( 'w', ['less', 'watch'] );
	grunt.registerTask( 'o', ['optimize'] );
	grunt.registerTask( 'f', ['ftp'] );
	grunt.registerTask( 'r', ['rsync'] );
	grunt.registerTask( 'd', ['default'] );
};
