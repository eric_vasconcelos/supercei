$(function() {

	var directionsService = new google.maps.DirectionsService(),
		route             = false,
		map,
		boxStores         = $(".address-stores"),
		stores            = boxStores.find(".loja"),
  		baseUrl           = window.location.origin,
        image 			  = baseUrl + '/testeBendita2014/wp-content/themes/supercei/assets/images/marker.png'; // marcadores - pontos no mapa

	// Criando o Array com a estilização do mapa
	var styles = [
  		{
	      featureType: "landscape",
	      elementType: "geometry.fill",
	      stylers: [
	        { color: "#e1c7a6" }
	      ]
	    },{
	      featureType: "road",
	      elementType: "geometry",
	      stylers: [
	        { color: "#eae1d6" }
	      ]
	    },{
	      featureType: "road",
	      elementType: "labels.text.fill",
	      stylers: [
	        { color: "#b29a77" }
	      ]
	    }
  	];
  	// Criando o estilo do mapa e passando o array
  	// colocando o nome que será exibido
  	var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});
  	
  	// Função que chama a latitude e longitudo desejada no mapa
	var initialize = function (latng1, latng2) {
	  	
	    directionsDisplay = new google.maps.DirectionsRenderer();
	    geocoder = new google.maps.Geocoder();
	    var myLatlng = new google.maps.LatLng(latng1, latng2);
	    var myOptions = {
	        zoom: 16,
	        center: myLatlng,
	        mapTypeControl: false,
	        mapTypeId: google.maps.MapTypeId.ROADMAP,
	        mapTypeControlOptions: {
		      mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
		    }
	    }
	    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

	    // Associando a estilização do mapa com o MapTypeId e mostrando no mapa
	  	map.mapTypes.set('map_style', styledMap);
	  	map.setMapTypeId('map_style');

	    // listando os endereços das lojas 
	    var places  = boxStores.find(".loja");
	    var attr = boxStores.find("address").attr('data-pos');

	    // identifica se já foi feita a criação dos pontos no mapa de acordo com o atributo "data-pos"
		if (typeof attr !== typeof undefined && attr !== false) {

		   // Passa por cada endereço com as coordendas disponibilizados no front 
		    for (var i = 0; i < places.length; i++) {

	    		var address  = $(places[i]).find("address").text(), // Pega o endereço
			    	position = $(places[i]).find("address").attr("data-pos"), // Pega as coordenadas no data-pos
			    	pos      = position.split(","); // cria um array das coordenadas

	    		// Adiciona todos os endereços e data-pos
	    		$(places[i]).find("address").remove();
	    		$(places[i]).append($("<address>").text(address).attr("data-pos", position));

		    	// Coloca os markers em cada posição
		    	var myLatLng 		= new google.maps.LatLng(pos[0], pos[1]),
			    	triartisMarker  = new google.maps.Marker({
				        position: myLatLng,
				        map: map,
				        icon: image
				    });
		    };

		} else {

			// Passa por cada endereço com as coordendas disponibilizados no front 
		    for (var i = 0; i < places.length; i++) {

	    		var addressArray = $(places[i]).find("address").text().trim().split(", "), // Pega o endereço e cria um array
			    pos          = addressArray.slice(-2), // pega os dois ultimos itens do array, que são as coordenadas
		    	address      = addressArray.slice(0, -3).join(", "); // pega o array, retira os três últimos itens que são desnecessários e recria o endereço
	    		
	    		// Adiciona todos os endereços modificados de volta
	    		$(places[i]).find("address").text(address).attr("data-pos", pos);

		    	// Coloca os markers em cada posição
		    	var myLatLng 		= new google.maps.LatLng(pos[0], pos[1]),
			    	triartisMarker  = new google.maps.Marker({
				        position: myLatLng,
				        map: map,
				        icon: image
				    });
		    };
		}

	    // seta os endereços nos marcadores 
	    directionsDisplay.setMap(map);
	    directionsDisplay.setPanel(document.getElementById("directionsPanel"));
	}

	var showStores = function () {
		// Mostra somente as lojas da cidade selecionada inicialmente
		var value  = $("#select-cidades-bot option:selected").val();
		boxStores.find(".id-"+ value).css("display", "block");

		//Mostra as lojas de acordo com a cidade escolhida e vai para o marker da primeira loja
		$("#select-cidades-bot").change(function () {
			var value  = $("#select-cidades-bot option:selected").val();

			boxStores.find(".loja").css("display", "none");
			boxStores.find(".id-"+ value).css("display", "block");
		});
	}
	showStores();
		

	var currentPosition = function () {
		// identifica se já foi feita a criação dos pontos no mapa de acordo com o atributo "data-pos"
		var attr = boxStores.find("address").attr('data-pos');

		if (typeof attr !== typeof undefined && attr !== false) {
			// Posiona o mapa no primeiro marcador da lista de endereços
			var pos = boxStores.find(".loja:visible:first address").attr("data-pos").split(", ");

		} else {
			// Posiona o mapa no primeiro marcador da lista de endereços
			var place = boxStores.find(".loja:visible:first address").text().trim().split(", "),
				pos   = place.slice(-2);
		}

	    initialize(pos[0], pos[1]);
	}
	currentPosition();


	var showClickedMarker = function () {
		// Quando clica no endereço, mostra a loja no mapa
		$(".loja").on("click", "address", function () {
			var pos = $(this).attr("data-pos").split(",");

			initialize(pos[0], pos[1]);
		});
	}
	showClickedMarker();

});
