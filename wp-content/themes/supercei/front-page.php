<?php
get_header(); ?>
	
	<div id="home-carousel">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="wrap-carousel">
						<?php echo do_shortcode("[metaslider id=17]"); ?>
					</div><!-- .wrap-carousel -->
				</div><!-- .col-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #home-carousel -->

	<div id="mid-home">
		<div class="container">
			<div class="row">

				<div class="col-6">
					<div class="video-home">

						<?php 
							$args = array( 'post_type' => 'video', 'posts_per_page' => 1 );
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>
								
								<?php the_content(); ?>
						<?php endwhile; ?>

					</div>
				</div><!-- .col-6 -->


				<div class="col-6">
					<div class="day-offer">
						<div class="row">
							<?php 
								$args = array( 'post_type' => 'oferta', 'posts_per_page' => 2 );
								$loop = new WP_Query( $args );
								while ( $loop->have_posts() ) : $loop->the_post(); ?>
									<div class="col-6">
										<a class="offer" href="<?php the_field('link_offer'); ?>" title="<?php the_title(); ?>">
											<?php the_post_thumbnail(array(222,256)); ?>
										</a>
									</div>
							<?php endwhile; ?>
						</div><!-- .row -->
					</div><!-- .day-offer -->
				</div><!-- .col-6 -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #mid-home -->

	<div id="maps">
		<div class="container">
			<div class="row">

				<div class="col-9">
					<div id="map_canvas" style="width:100%; height:300px"></div>
				</div><!-- .col-6 -->
				
				<div class="col-3">
					<div class="location">
						<p>você está em </p>
						<div class="wrap-select">
							<?php 
								$args = array(
									'show_option_all'    => '',
									'show_option_none'   => '',
									'orderby'            => 'NAME', 
									'order'              => 'ASC',
									'show_count'         => 0,
									'hide_empty'         => 1, 
									'child_of'           => 0,
									'exclude'            => '',
									'echo'               => 1,
									'selected'           => 0,
									'hierarchical'       => 0, 
									'name'               => 'cidades',
									'id'                 => 'select-cidades-bot',
									'class'              => 'cities',
									'depth'              => 0,
									'tab_index'          => 0,
									'taxonomy'           => 'cidade',
									'hide_if_empty'      => true,
								); 

								wp_dropdown_categories( $args );
							?>
						</div>
					</div>
					<div class="address-stores">

						<?php
							$args = array( 'post_type' => 'lojas', 'posts_per_page' => -1 );
							$loop = new WP_Query( $args );
							while ( $loop->have_posts() ) : $loop->the_post(); ?>

							<?php $terms = get_the_terms( $post->ID, 'cidade' ); ?>
							<div class="loja cf<?php foreach( $terms as $term ) echo ' id-' . $term->term_id; ?>">
								<i class="marker"></i>
								<address><?php the_field('mapa');  ?></address>
							</div>

						<?php endwhile; ?>
					</div>
				</div><!-- .col-3 -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #mid-home -->
				
<?php
get_footer();
