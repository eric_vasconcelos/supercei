/*
Theme Name: Bootstrap
Theme URI: http://wordpress.org/
Description: Tema criado para sites institucionais de grandes empresas
Author: Eric Vasconcelos
Author URI: http://facebook.com/eric.v.deoliveira
Version: 1.0
*/


<!-- Importar estilos do bootstrap -->
@import url('bootstrap/css/bootstrap.css');
@import url('bootstrap/css/bootstrap-responsive.css');

<!-- Head para wordpress -->
<title><?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>
<meta name="generator" content="WordPress <?php bloginfo('versrion'); ?>" />
<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; <?php bloginfo('charset'); ?>;" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" media="all" type="text/css" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />

<!-- Importar estilo do tema para bootstrap -->
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">

<!-- ****************************************************************** -->

<!-- Funções php Wordpress-->
<!-- Puxar menu do wordpress -->

<?php wp_head(); ?>

<!-- Puxar header.php -->

<?php get_header(); ?>

<!-- puxar o footer.php -->

<?php get_footer(); ?>

<!-- Puxar o titulo da página sem o '>>' -->

<?php wp_title("",true); ?>

<!-- ****************************************************************** -->

<!-- SIDEBAR-->
<?php get_sidebar(); ?>
	<?php 
	// Registrar a sidebar - colocar no functions.php
	if(function_exists('register_sidebar'))
		register_sidebar(array(
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '<h2>',
			'after_title' => '</h2>',
		));
	?>

	<!-- ou -->

		<?php 
			if(function_exists('register_sidebar'))
				register_sidebar(array(
					'before_widget' => '<div class="widgets">',
					'after_widget' => '</div>',
					'before_title' => '<h2>',
					'after_title' => '</h2>',
				));
		?>
    <!-- Colocar no sidebar.php-->
	<div class="sidebar">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar()) : ?>
		<?php endif; ?>
	</div>

<!-- ****************************************************************** -->

	<!-- Puxar os posts e seus dados -->
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	    <h1><?php the_title(); ?></h1>
	    <?php the_content(); ?>
     	<?php the_excerpt(); ?>
     	<?php comments_template(); ?>
     	<?php the_permalink(); ?>
     	<?php the_author(); ?>
     	<?php the_date('d/m/y'); ?>
     	<?php the_time('g:i a'); ?>
     	<?php the_post_thumbnail(array(width, height)); ?>

     	<!-- Adicionar a função do thumbnails no functions.php -->
     	<?php add_theme_support('post-thumbnails'); ?>
     	<?php comments_number("Nenhum comentário", "1 comentário", "% comentários"); ?>
     	<?php the_category(""); ?>
     	<?php the_author(); ?>

        <div class="nav-previous alignleft"><?php next_posts_link( 'Antigos' ); ?></div>
        <div class="nav-next alignright"><?php previous_posts_link( 'Novos' ); ?></div>

    <?php endwhile; else: ?>
	    <p>Nenhum post encontrado!</p>
    <?php endif; ?>

    <!-- Puxar os posts de uma categoria -->

    <!-- o número indica o id da categoria que os posts serão puxados, este funcao só serve para os posts normais e nao para os posts type -->
    <?php query_posts("{$query_string}&cat=-0"); ?> 
    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    <?php endwhile; else: ?>
    <?php endif; ?>

    <!-- Puxar os ultimos posts -->
    <?php $aRecentPosts = new WP_Query("showposts=5"); // 4 é o número de posts recentes que você deseja mostrar
    while($aRecentPosts->have_posts()) : $aRecentPosts->the_post();?>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    <?php endwhile; ?>

<!-- ****************************************************************** -->

    <!-- CUSTOM POST TYPE -->
    <!-- Puxar post type -->
    <?php
    $args = array( 'post_type' => 'colocar-aqui-slug', 'posts_per_page' => 5/*número de posts para aparecer*/ );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();
    ?>
        <?php the_post_thumbnail(array(width, height)); ?>
        <?php the_title(); ?>
        <?php the_excerpt(); ?>
    <?php endwhile; ?>

    <!-- Passar página -->
    <div class="nav-previous alignleft"><?php next_posts_link( 'Antigos' ); ?></div>
    <div class="nav-next alignright"><?php previous_posts_link( 'Novos' ); ?></div>


    <!-- Puxar lista de post type de uma categoria de uma taxonomia -->
    <?php
        $args = array(
            'post_type' => 'COLOCAR SLUG DO POST TYPE',
            'tax_query' => array(
                array(
                    'taxonomy' => 'NOME DA TAXONOMIA',
                    'field' => 'slug',
                    'terms' => 'CATEGORIA DA TAXONOMIA'
                )
            )
        );


        $nomePostType = new WP_Query( $args );
        if( $nomePostType->have_posts() ) {
            while( $nomePostType->have_posts() ) {
                $nomePostType->the_post();
                ?>
                    <div class="col-lg-8">
                        <div class="member-team">
                            <div class="img-team"><?php the_post_thumbnail(array(400, 400)); ?></div>
                            <div class="content-tem">
                                <h1><?php the_title(); ?></h1>
                                <p><?php the_excerpt(); ?></p>
                            </div>
                        </div>
                    </div>
                <?php
            }
        }
        else {
            echo 'Nada!';
        }
    ?>




    <!-- Taxonomy - Puxar a lista de categorias de uma taxonomia-->
    <?php
        $terms = get_terms('nome-da-taxonomia');
            foreach ($terms as $term) {

                //Always check if it's an error before continuing. get_term_link() can be finicky sometimes
                $term_link = get_term_link( $term, 'nome-da-taxonomia' );
                if( is_wp_error( $term_link ) )
                    continue;
                //We successfully got a link. Print it out.
                echo '<div class="class-exemplo" >
                    <h4>
                        <a href="' . $term_link . '">' . $term->name . '</a>
                    </h4>
                </div>';
                }
    ?>

    <!-- puxar imagens da taxonomia... válido apenas quando usado no archive.php-->
    <?php print apply_filters( 'taxonomy-images-queried-term-image', '' ); ?>
        </div>
            <div id="tab2" class="tab-pane">
             <?php 
              $args = array(
            'post_type' => 'informacao',
            'tax_query' => array(
             array(
              'taxonomy' => 'tipo',
              'field' => 'slug',
              'terms' => 'hoteis',
              'posts_per_page' => -1
             )
            )
           );
           $query = new WP_Query( $args );
           if ($query->have_posts()): while ($query->have_posts()): $query->the_post();
          ?>
          <div class="bloco-info">
           <div class="txt-informacao">
              <h4><?php the_title(); ?></h4>
              <p><strong><?php the_content(); ?></strong></p><br />
             </div>
          </div>

          <?php endwhile; endif; ?>
        </div>


<!-- Puxar informações de uma página -->

<?php if ( 'ID' ) { // acrescentar o id da página
 $post = get_post( 'ID' );
 if ( $post ) { ?>

<?php echo get_the_post_thumbnail('98', 'full'); ?> 
<?php echo $post->post_title; ?>
<?php echo wp_trim_words( $post->post_excerpt ); ?>

<?php } } ?>

<!-- Ativar resumo(excerpt) em páginas no function.php -->
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

 <!-- ================================================== -->

<!-- add no function -->
<!-- Modificar o tamanho do excerpt -->
function twentyten_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'twentyten_excerpt_length', 100 ); substituir o número


<!-- Puxar posts de uma categoria  -->

$args = array(
    'post_type' => 'nome-post-tye',
    'tax_query' => array(
        array(
            'taxonomy' => 'nome-taxonomia',
            'field' => 'slug',
            'terms' => 'nome-categoria'
        )
    )
);
$posttype = new WP_Query( $args );
if( $posttype->have_posts() ) {
    while( $posttype->have_posts() ) {
        $posttype->the_post();
        ?>
            <div class="col-lg-8">
                <div class="treatment">
                    <div class="img-treatment"><?php the_post_thumbnail(array(400, 400)); ?></div>
                    <div class="content-treatment">
                        <h3><?php the_title(); ?></h3>
                        <p><?php the_content(); ?></p>
                    </div>
                </div>
            </div>
        <?php
    }
}
else {
    echo 'Nada!';
}
?>


<!-- Listar a quantidade de post por página de cada taxonomia ou termo de taxonomia  -->

function filter_press_tax( $query ){
    if( $query->is_tax('press') && $query->has_term('press')):
        $query->set('posts_per_page', 5);
        return;
    endif;
}


<!-- Funcionar tags nos post types -->
function wpse28145_add_custom_types( $query ) {
    if( is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

        // this gets all post types:
        $post_types = get_post_types();

        // alternately, you can add just specific post types using this line instead of the above:
        // $post_types = array( 'post', 'your_custom_type' );


        $query->set( 'post_type', $post_types );
        return $query;
    }
}
add_filter( 'pre_get_posts', 'wpse28145_add_custom_types' );



<!-- ****************************************************************** -->

    <!-- Puxar a lista de categorias ou páginas -->
    <li><a href="<?php echo get_option('home'); ?>">Home</a></li>
	<?php wp_list_categories('title_li='); ?>
	<?php wp_list_pages('title_li='); ?>

    <!-- Searchform.php - Formulário de busca -->
	<form action="" method="get" accept-charset="utf-8" id="searchform" role="search">
		<div>
			<input type="text" name="s" id="s" value="Pesquisar no site" onblur="if(this.value=='') this.value='Pesquisar no site';"
			onfocus="if(this.value=='Pesquisar no site') this.value=''; " />
			<input type="submit" id="searchsubmit" value="Buscar" />
		</div>
	</form>

<!-- Twitter Bootstrap -->
	<!-- Colocar no head -->
<?php wp_enqueue_script('jquery'); ?>
<?php 
//Fazer o javascrpit funcionar - colocar no functions.php
function bootpress_scripts_with_jquery () {

	//register the script like this for a theme;
	wp_register_script( 'custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.js' , array( 'jquery' ) );

	// for either a plugin or a theme, you can then enqueue the script;
	wp_enqueue_script( 'custom-script' );
    }
    add_action('wp_enqueue_scripts', 'bootpress_scripts_with_jquery');
?>

	<!-- Colocar no header o menu do site -->
    <?php wp_nav_menu(array( 'theme_location' => 'header-menu')); ?>
    <?php 
    // Registrar o menu principal no painel do wordpress (colocar no functions.php)
    function register_my_menu () {
    	register_nav_menu( 'header-menu',__( 'Header menu'));
    }
    add_action( 'init', 'register_my_menu')
    ?>


<!-- Páginas -->
index.php
style.css
header.php
footer.php
sidebar.php
functions.php
front-page.php
page.php
single.php
home.php
searchform.php
404.php


<!-- LOOPS -->
<?php
  /* Run the loop to output the posts.
   * If you want to overload this in a child theme then include a file
   * called loop-index.php and that will be used instead.
   */
  get_template_part( 'loop', 'index' );
?>