
	<?php $contact_opts = get_option( 'odin_general' ); ?>

	<footer id="footer" class="cf" role="contentinfo">

		<div class="footer-menu">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'main-menu',
									'depth'          => 1,
									'container'      => false,
									'menu_class'     => 'nav'
								)
							);
						?>
					</div>
				</div><!-- .row-->
			</div><!-- .container-->
		</div>

		<div class="footer-bot">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<p class="copyright"><?php echo $contact_opts['copyright'];?></p><!-- .copyright -->
					</div>
				</div><!-- .row-->
			</div><!-- .container-->
		</div>
		
	</footer><!-- #footer -->
	
	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5.js"></script>
	<![endif]-->
	<?php wp_footer(); ?>

</body>
</html>
