<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

	<div id="primary">
		<div id="content" class="site-content" role="main">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
							
							<!-- títitulo -->
							<h1><?php the_title(); ?></h1>
							
							<!-- Texto 1 -->
							<div class="texto1"><?php the_field('texto1'); ?></div>
							
							<!-- Imagem central -->
							<div class="img-single">
								<img src="<?php the_field('imagem_central'); ?>" alt="<?php the_title(); ?>">
							</div>
							
							<!-- Texto 1 -->
							<div class="texto2"><?php the_field('texto2'); ?></div>

						<?php endwhile; else: ?>
						    <p>Nenhum conteúdo encontrado!</p>
					    <?php endif; ?>
					</div><!-- .col-12 -->
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_footer(); ?>
