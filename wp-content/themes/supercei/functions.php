<?php
/**
 * Sets content width.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 600;
}
/**
 * Odin Classes.
 */
require_once get_template_directory() . '/core/classes/class-bootstrap-nav.php';
require_once get_template_directory() . '/core/classes/class-shortcodes.php';
// require_once get_template_directory() . '/core/classes/class-thumbnail-resizer.php';
require_once get_template_directory() . '/core/classes/class-theme-options.php';
require_once get_template_directory() . '/core/classes/class-options-helper.php';
require_once get_template_directory() . '/core/classes/class-post-type.php';
require_once get_template_directory() . '/core/classes/class-taxonomy.php';
// require_once get_template_directory() . '/core/classes/class-metabox.php';
// require_once get_template_directory() . '/core/classes/abstracts/abstract-front-end-form.php';
// require_once get_template_directory() . '/core/classes/class-contact-form.php';
// require_once get_template_directory() . '/core/classes/class-post-form.php';
// require_once get_template_directory() . '/core/classes/class-user-meta.php';


/**
 * Odin Widgets.
 */
require_once get_template_directory() . '/core/classes/widgets/class-widget-like-box.php';
if ( ! function_exists( 'odin_setup_features' ) ) {
	/**
	 * Setup theme features.
	 *
	 * @since  2.2.0
	 *
	 * @return void
	 */
	function odin_setup_features() {
		/**
		 * Add support for multiple languages.
		 */
		load_theme_textdomain( 'odin', get_template_directory() . '/languages' );
		/**
		 * Register nav menus.
		 */
		register_nav_menus(
			array(
				'main-menu' => __( 'Main Menu', 'odin' )
			)
		);
		/*
		 * Add post_thumbnails suport.
		 */
		add_theme_support( 'post-thumbnails' );
		/**
		 * Add feed link.
		 */
		add_theme_support( 'automatic-feed-links' );
		/**
		 * Support Custom Header.
		 */
		$default = array(
			'width'         => 0,
			'height'        => 0,
			'flex-height'   => false,
			'flex-width'    => false,
			'header-text'   => false,
			'default-image' => '',
			'uploads'       => true,
		);
		add_theme_support( 'custom-header', $default );
		/**
		 * Support Custom Background.
		 */
		$defaults = array(
			'default-color' => '',
			'default-image' => '',
		);
		add_theme_support( 'custom-background', $defaults );
		/**
		 * Support Custom Editor Style.
		 */
		add_editor_style( 'assets/css/editor-style.css' );
		/**
		 * Add support for infinite scroll.
		 */
		add_theme_support(
			'infinite-scroll',
			array(
				'type'           => 'scroll',
				'footer_widgets' => false,
				'container'      => 'content',
				'wrapper'        => false,
				'render'         => false,
				'posts_per_page' => get_option( 'posts_per_page' )
			)
		);
		/**
		 * Add support for Post Formats.
		 */
		// add_theme_support( 'post-formats', array(
		//     'aside',
		//     'gallery',
		//     'link',
		//     'image',
		//     'quote',
		//     'status',
		//     'video',
		//     'audio',
		//     'chat'
		// ) );
		/**
		 * Support The Excerpt on pages.
		 */
		// add_post_type_support( 'page', 'excerpt' );
	}
}
add_action( 'after_setup_theme', 'odin_setup_features' );
/**
 * Register widget areas.
 *
 * @since  2.2.0
 *
 * @return void
 */
function odin_widgets_init() {
	register_sidebar(
		array(
			'name' => __( 'Main Sidebar', 'odin' ),
			'id' => 'main-sidebar',
			'description' => __( 'Site Main Sidebar', 'odin' ),
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widgettitle widget-title">',
			'after_title' => '</h3>',
		)
	);
}
add_action( 'widgets_init', 'odin_widgets_init' );
/**
 * Flush Rewrite Rules for new CPTs and Taxonomies.
 *
 * @since  2.2.0
 *
 * @return void
 */
function odin_flush_rewrite() {
	flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'odin_flush_rewrite' );
/**
 * Load site scripts.
 *
 * @since  2.2.0
 *
 * @return void
 */

function odin_enqueue_scripts() {
	$template_url = get_template_directory_uri();

	// Loads Odin main stylesheet.
	wp_enqueue_style( 'odin-style', get_stylesheet_uri(), array(), null, 'all' );

	// General scripts.
    // jQuery.
    wp_deregister_script('jquery');
    wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-1.11.1.min.js', array(), null, true);

    wp_enqueue_script( 'apimaps', 'http://maps.googleapis.com/maps/api/js?sensor=true', array(), null, true);

	// Main JS
	wp_enqueue_script( 'mainjs', $template_url . '/assets/js/main.min.js', array(), null, true );

    // Load Thread comments WordPress script.
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'odin_enqueue_scripts', 1 );

/**
 * Odin custom stylesheet URI.
 *
 * @since  2.2.0
 *
 * @param  string $uri Default URI.
 * @param  string $dir Stylesheet directory URI.
 *
 * @return string      New URI.
 */
function odin_stylesheet_uri( $uri, $dir ) {
	return $dir . '/assets/css/style.css';
}
add_filter( 'stylesheet_uri', 'odin_stylesheet_uri', 10, 2 );
/**
 * Core Helpers.
 */
require_once get_template_directory() . '/core/helpers.php';
/**
 * WP Custom Admin.
 */
require_once get_template_directory() . '/inc/admin.php';
/**
 * Comments loop.
 */
// require_once get_template_directory() . '/inc/comments-loop.php';
/**
 * WP optimize functions.
 */
require_once get_template_directory() . '/inc/optimize.php';
/**
 * WP Custom Admin.
 */
require_once get_template_directory() . '/inc/plugins-support.php';
/**
 * Custom template tags.
 */
// require_once get_template_directory() . '/inc/template-tags.php';

/* Post type Vídeo */ 
function odin_video_cpt() {
    $video = new Odin_Post_Type(
        'Vídeo', // Nome (Singular) do Post Type.
        'video' // Slug do Post Type.
    );
    $video->set_labels(
        array(
            'menu_name' => __( 'Vídeos', 'odin' ),
            'all_items' => 'Todos os Vídeos',
            'add_new' => 'Adicionar novo',
            'add_new_item' => 'Adicionar novo Video',
            'edit_item' => 'Editar Video',
            'new_item' => 'Novo Video',
            'view_item' => 'Ver Vídeos',
            'search_items' => 'Buscar Vídeos',
            'not_found' => 'Nada encontrado',
            'not_found_in_trash' => 'Nada encontrado na lixeira'
        )
    );
    $video->set_arguments(
        array(
            'supports' => array( 'title', 'editor' ),
            'rewrite'  => true,
            'exclude_from_search' => true,
            'menu_position' => 4
        )
    );
}
add_action( 'init', 'odin_video_cpt', 1 );

/* Post type Oferta */ 
function odin_oferta_cpt() {
    $oferta = new Odin_Post_Type(
        'Oferta', // Nome (Singular) do Post Type.
        'oferta' // Slug do Post Type.
    );
    $oferta->set_labels(
        array(
            'menu_name' => __( 'Ofertas', 'odin' ),
            'all_items' => 'Todas os Ofertas',
            'add_new' => 'Adicionar nova',
            'add_new_item' => 'Adicionar nova Oferta',
            'edit_item' => 'Editar Oferta',
            'new_item' => 'Nova Oferta',
            'view_item' => 'Ver Ofertas',
            'search_items' => 'Buscar Ofertas',
            'not_found' => 'Nada encontrado',
            'not_found_in_trash' => 'Nada encontrado na lixeira'
        )
    );
    $oferta->set_arguments(
        array(
            'supports' => array( 'title', 'thumbnail' ),
            'rewrite'  => true,
            'exclude_from_search' => true,
            'menu_position' => 5
        )
    );
}
add_action( 'init', 'odin_oferta_cpt', 1 );

/* Post type Lojas */ 
function odin_lojas_cpt() {
    $oferta = new Odin_Post_Type(
        'Loja', // Nome (Singular) do Post Type.
        'lojas' // Slug do Post Type.
    );
    $oferta->set_labels(
        array(
            'menu_name' => __( 'Lojas', 'odin' ),
            'all_items' => 'Todas as Lojas',
            'add_new' => 'Adicionar nova',
            'add_new_item' => 'Adicionar nova Loja',
            'edit_item' => 'Editar Loja',
            'new_item' => 'Nova Loja',
            'view_item' => 'Ver Lojas',
            'search_items' => 'Buscar Lojas',
            'not_found' => 'Nada encontrado',
            'not_found_in_trash' => 'Nada encontrado na lixeira'
        )
    );
    $oferta->set_arguments(
        array(
            'supports' => array( 'title'),
            'rewrite'  => true,
            'exclude_from_search' => true,
            'menu_position' => 5
        )
    );
}
add_action( 'init', 'odin_lojas_cpt', 1 );

/* Taxonomia cidade das lojas */ 
function odin_cidade_taxonomy() {
    $video = new Odin_Taxonomy(
        'Cidade', // Nome (Singular) da nova Taxonomia.
        'cidade', // Slug do Taxonomia.
        'lojas' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
    );

    $video->set_labels(
        array(
            'menu_name' => __( 'Cidades', 'odin' )
        )
    );

    $video->set_arguments(
        array(
            'hierarchical' => true
        )
    );
}
add_action( 'init', 'odin_cidade_taxonomy', 1 );

// Opções do tema
function odin_theme_settings_example() {

    $settings = new Odin_Theme_Options(
        'odin-settings', // Slug/ID of the Settings Page (Required)
        'Opções do Tema', // Settings page name (Required)
        'manage_options' // Page capability (Optional) [default is manage_options]
    );

    $settings->set_tabs(
        array(
            array(
                'id' => 'odin_general', // Slug/ID of the Settings tab (Required)
                'title' => __( 'Supercei', 'odin' ), // Settings tab title (Required)
            )
        )
    );

    $settings->set_fields(
        array(
            'odin_general_fields_section' => array( // Slug/ID of the section (Required)
                'tab'   => 'odin_general', // Tab ID/Slug (Required)
                'title' => __( 'Informações', 'odin' ), // Section title (Required)
                'fields' => array( // Section fields (Required)

                    // Textarea Field.
                    array(
                        'id'          => 'copyright', // Required
                        'label'       => __( 'Copyright', 'odin' ), // Required
                        'type'        => 'textarea', // Required
                        'attributes'  => array( // Optional (html input elements)
                            'placeholder' => __( 'Escreva o copyright que vai aparecer no rodapé do site' )
                        )
                    )
                )
            )
        )
    );
}

add_action( 'init', 'odin_theme_settings_example', 1 );


// Retirando o large size thumbnail
function wpmayor_filter_image_sizes( $sizes) {
    unset( $sizes['medium']);
    unset( $sizes['large']);
     
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'wpmayor_filter_image_sizes');
 

add_filter( 'jpeg_quality', create_function( '', 'return 100;' ) );

